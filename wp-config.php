<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'mondial_textile');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`:%pKZjsEZQL6dbllY$_pZC(i#y9Uo(*m6S xuc8*`tG-c/4Al9t*:ugT|?aZ)5E');
define('SECURE_AUTH_KEY',  '0O^;r^5>rF }]<>E.r+AkaeWh.6X1#]|o0sX-yuoqjz}/AVx3C:H*NP*i}nEsYa ');
define('LOGGED_IN_KEY',    'WC~W<h@ Ll`-#81Pg;ky<_.x.c8W!=`VELPZU{]S?H+*)+4#8-8-XE?o+We-bI.f');
define('NONCE_KEY',        ' Ip(xN0cKhP(=!PG@}Vqr,d#<M-g3jO+i7ZGY9GBd++q_|$8Rv&G>r/qm;+M!R}(');
define('AUTH_SALT',        'v|~u7l-+,]N(3D8]sq=7%u<`<$DFF9j2r|+wo>6?~<5 |wiSYjWU+h<,L34F[?6u');
define('SECURE_AUTH_SALT', '6!BQTSbaeZ)L@M<^yW{MF|nF_7&SX{/:5HIxRzn6~| Bh}x%w07X:t)dUEq^9s)r');
define('LOGGED_IN_SALT',   '.s1-!+}7Xy3f[+0hgct|gG?km&?I`@r2,4uP+~a+PJga_6,;]#I2yMHMf9=}.b(H');
define('NONCE_SALT',       'GeY jW>@DR~p^DgyFx/nN}15[q 3`%g@|/G)6ZZJ3JMW3ZH&f6J|Uie+Ffa.ds1r');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Langue de localisation de WordPress, par défaut en Anglais.
 *
 * Modifiez cette valeur pour localiser WordPress. Un fichier MO correspondant
 * au langage choisi doit être installé dans le dossier wp-content/languages.
 * Par exemple, pour mettre en place une traduction française, mettez le fichier
 * fr_FR.mo dans wp-content/languages, et réglez l'option ci-dessous à "fr_FR".
 */
define('WPLANG', 'fr_FR');

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');