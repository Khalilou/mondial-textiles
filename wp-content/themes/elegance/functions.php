<?php
@ini_set( 'upload_max_size' , '2G' );
@ini_set( 'post_max_size', '5G');
@ini_set( 'max_execution_time', '600' );
@ini_set( 'memory_limit', '2G' );
@ini_set( 'max_input_vars', '100000' );

/**
 * Sets up the theme by loading the Mysitemyway class & initializing the framework
 * which activates all classes and functions needed for theme's operation.
 *
 * @package Mysitemyway
 * @subpackage Functions
 */

# Load the Mysitemyway class.
require_once( TEMPLATEPATH . '/framework.php' );

# Get theme data.
$theme_data = get_theme_data( TEMPLATEPATH . '/style.css' );

# Initialize the Mysitemyway framework.
Mysitemyway::init(array(
	'theme_name' => $theme_data['Name'],
	'theme_version' => $theme_data['Version']
));

// Our custom post type function
function create_posttype() {

	register_post_type( 'popup-home',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'popup-home' ),
				'singular_name' => __( 'Popup Accueil' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'popup-home'),
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

function custom_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Popups', 'Post Type General Name', 'twentythirteen' ),
		'singular_name'       => _x( 'Popup', 'Post Type Singular Name', 'twentythirteen' ),
		'menu_name'           => __( 'Popups', 'twentythirteen' ),
		'parent_item_colon'   => __( 'Parent Popup', 'twentythirteen' ),
		'all_items'           => __( 'All Popups', 'twentythirteen' ),
		'view_item'           => __( 'View Popup', 'twentythirteen' ),
		'add_new_item'        => __( 'Add New Popup', 'twentythirteen' ),
		'add_new'             => __( 'Add New', 'twentythirteen' ),
		'edit_item'           => __( 'Edit Popup', 'twentythirteen' ),
		'update_item'         => __( 'Update Popup', 'twentythirteen' ),
		'search_items'        => __( 'Search Popup', 'twentythirteen' ),
		'not_found'           => __( 'Not Found', 'twentythirteen' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
	);
	}
	
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

add_action( 'init', 'custom_post_type', 0 );
	
?>