<?php
/**
 * Home Template
 *
 * @package Mysitemyway
 * @subpackage Template
 */

get_header(); 

$post_obj = $wp_query->get_queried_object();
?>

<?php if ( ( mysite_get_setting( 'frontpage_blog' ) ) || ( !empty( $post_obj->ID ) && get_option('page_for_posts') == $post_obj->ID ) ) : ?>
	<?php get_template_part( 'loop', 'index' ); ?>
	
<?php endif; ?>

<?php mysite_after_page_content(); ?>

		<div class="clearboth"></div>
	</div><!-- #main_inner -->
</div><!-- #main -->

<?php get_footer(); ?>

<?php 




$timetoday = date('Ymd');

$loop = new WP_Query( array( 
    'post_type' => 'popup-home',
    'post_status' => 'publish',
	'meta_query'		=> array(
		array(
			'key' => 'date_de_fin_de_publication',
			'compare' => '>',
			'value' => $timetoday
		)
	),
	'meta_key' => 'date_de_fin_de_publication', // name of custom field
    'ignore_sticky_posts' => 1,
    'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1 ),
    'nopaging' => 1
	) );
	
	if ( $loop->have_posts() ) :
    while ( $loop->have_posts() ) : $loop->the_post();
	?>
		<div class="modal">
			<div class="modal-container">
				<div class="modal-content">
					<?php if ( get_field('lien-popup') ): ?> 
						<a href="<?php the_field('lien-popup'); ?>">
					<?php endif; ?>	
						<img alt="<?php echo get_the_title(); ?>" src="<?php the_field('image-popup'); ?>" />
						<h1 style="color:white;"><?php echo get_the_title(); ?></h1>
					<?php if ( get_field('lien-popup') ): ?> 
						</a>
					<?php endif; ?>	
				</div>
				
			</div>
			<div class="modal-overlay"></div>
		</div>
	
	<?php 
	
		
	endwhile;
    ?>
    
	<script type="text/javascript">
		jQuery(".is_home .modal").fadeIn();
		  jQuery(".modal").click(function(){
		  jQuery(this).fadeOut();
		});
	</script>
	
	<?php     
    endif;
		
?>